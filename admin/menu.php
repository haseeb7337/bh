<div id="sidebar-menu">

	<ul class="metismenu" id="side-menu">

		<!--<li class="menu-title">Navigation</li>-->

		<li>
			<a href="dashboard.php">
				<i class="fi-air-play"></i><span class="badge badge-danger badge-pill pull-right">7</span> <span> Dashboard </span>
			</a>
		</li>

	
		
		<li>
			<a href="javascript: void(0);"><i class="fi-mail"></i><span> Manage Venues </span> <span class="menu-arrow"></span></a>
			<ul class="nav-second-level" aria-expanded="false">
				<li><a href="venues.php">View Venues</a></li>

				<li><a href="venue_type.php">Venue Types</a></li>
			</ul>
		</li>
		
		
		<li>
			<a href="javascript: void(0);"><i class="fi-mail"></i><span> Manage Events </span> <span class="menu-arrow"></span></a>
			<ul class="nav-second-level" aria-expanded="false">
				<li><a href="event.php">View Events</a></li>
				<li><a href="event_type.php">Event Types</a></li>
			</ul>
		</li>

		

	</ul>

</div>