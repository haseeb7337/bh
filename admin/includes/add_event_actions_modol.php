<div id="modal_theme_add_venue" class="modal fade bs-actions-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				
				<h4 class="modal-title" id="myLargeModalLabel">Update Event Category</h4>
			</div>
			<div class="modal-body">
			   
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" id="event_name" name="name" class="form-control" >
							</div>
						</div>	
					</div>

			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveVenue()" class="btn btn-primary" >Save</button>
				<button type="button" class="btn btn-warning"  data-dismiss="modal" >Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->