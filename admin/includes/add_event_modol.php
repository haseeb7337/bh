<div id="modal_theme_add_venue" class="modal fade bs-event-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				
				<h4 class="modal-title" id="myLargeModalLabel">Add New Event</h4>
			</div>
			<div class="modal-body">
			   
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Title</label>
								<input type="text" id="event_name" name="event_name" class="form-control" >
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Choose Photo </label>
								<input type="file" class="filestyle" data-btnClass="btn-primary">
							</div>
							
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Description</label>
								<textarea type="text" id="event_decp" name="event_decp" class="form-control" ></textarea>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Start Date: </label>
								<input type="date" id="event_start_date" name="event_start_date" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>End Date: </label>
								<input type="date" id="event_end_date" name="event_end_date" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Start Time: </label>
								<input type="time" id="event_s_time" name="event_s_time" class="form-control" >
							</div>
						</div>	
						<div class="col-md-6">
							<div class="form-group">
								<label>End Time: </label>
								<input type="time" id="event_e_time" name="event_e_time" class="form-control" >
							</div>
						</div>
					</div>
					
					<form id ="form_id" action = "DEFAULT_ACTION">	
					<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Select Venue:</label>
									<select id="venuenames" class="form-control select2">
																			
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Event Type:</label>
									<select class="form-control" id="eventtype" data-init-plugin="select2">
																			
									</select>
								</div>
								</div>
							</div>
					</form>
					

					
					
			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveEvent()" class="btn btn-primary" >Save</button>
				<button type="button" onclick="CloseModol()" class="btn btn-warning">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->