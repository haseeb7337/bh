<div id="modal_theme_add_venue" class="modal fade bs-time-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" id="myLargeModalLabel">Manage Venue Hours</h4>
			</div>
			<div class="modal-body">					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Monday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="emopen" name="emopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="emclose" name="emclose" class="form-control" >
							</div>
						</div>
					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Tuesday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="etopen" name="etopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="etclose" name="etclose" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Wednesday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="ewedopen" name="ewedopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="ewedclose" name="ewedclose" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Thursday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="ethopen" name="ethopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="ethclose" name="ethclose" class="form-control" >
							</div>
						</div>
					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Friday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="efriopen" name="efriopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="efriclose" name="efriclose" class="form-control" >
							</div>
						</div>
					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Saturday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="esatopen" name="esatopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="esatclose" name="esatclose" class="form-control" >
							</div>
						</div>
					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Sunday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="esunopen" name="esunopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="esunclose" name="esunclose" class="form-control" >
							</div>
						</div>
					</div>






			</div>
			<div class="modal-footer">
				<button type="button" onclick="updateTimes()" class="btn btn-primary" >Save</button>
				<button type="button" class="btn btn-warning"  data-dismiss="modal" >Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->