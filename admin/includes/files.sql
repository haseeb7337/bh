INSERT INTO `bh_venue_timmings`(`venue_time_id`, `venue_id`, `monday_open`, `monday_close`, `tuesday_open`, `tuesday_close`, `wednesday_open`, `wednesday_close`, `thursday_open`, `thursday_close`, `friday_open`, `friday_close`, `saturday_open`, `saturday_close`, `sunday_open`, `sunday_close`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13],[value-14],[value-15],[value-16])



UPDATE `bh_venue_timmings` SET `monday_open`=[value-3],`monday_close`=[value-4],
`tuesday_open`=[value-5],`tuesday_close`=[value-6],`wednesday_open`=[value-7],
`wednesday_close`=[value-8],`thursday_open`=[value-9],
`thursday_close`=[value-10],`friday_open`=[value-11],`friday_close`=[value-12],
`saturday_open`=[value-13],`saturday_close`=[value-14],
`sunday_open`=[value-15],`sunday_close`=[value-16] WHERE venue_id = '15'



UPDATE `bh_venues` 
SET    `venue_id` = [value-1], 
       `venue_name` = [value-2], 
       `venue_desc` = [value-3], 
       `venue_type_id` = [value-4], 
       `venue_street` = [value-5], 
       `venue_state` = [value-6], 
       `venue_city` = [value-7], 
       `venue_phone` = [value-8], 
       `venue_twitter` = [value-9], 
       `venue_insta` = [value-10], 
       `venue_facebook` = [value-11], 
       `venue_youtube` = [value-12], 
       `venue_website` = [value-13], 
       `venue_email` = [value-14], 
       `is_venue_claimed` = [value-15], 
       `user_id` = [value-16], 
       `venue_status` = [value-17], 
       `venue_created_date` = [value-18] 
WHERE  1 



INSERT INTO `bh_venues_event` 
            (`venue_event_id`, 
             `venue_event_title`, 
             `venue_event_pic`, 
             `venue_event_pic_cap`, 
             `venue_event_descp`, 
             `venue_event_start_date`, 
             `venue_event_end_date`, 
             `venue_event_multi_dates`, 
             `venue_event_start_time`, 
             `venue_event_end_time`, 
             `event_type_cat_id`, 
             `venue_id`, 
             `is_event_active`) 
VALUES      ([value-1], 
             [value-2], 
             [value-3], 
             [value-4], 
             [value-5], 
             [value-6], 
             [value-7], 
             [value-8], 
             [value-9], 
             [value-10], 
             [value-11], 
             [value-12], 
             [value-13]) 