<div id="modal_theme_add_venue" class="modal fade bs-example-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">

				<h4 class="modal-title" id="myLargeModalLabel">Add New Venue</h4>
			</div>
			<div class="modal-body">


					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Venue Name</label>
								<input type="text" id="venue_name" name="venue_name" class="form-control" >
							</div>
						</div>
					<!--	<form id ="form_id" action = "DEFAULT_ACTION">
								<div class="col-md-6">
									<div class="form-group">
										<label>Venue Type:</label>
										<select id="addVenueType">

										</select>
									</div>
								</div>
						</form> -->
						
						<div class="col-md-6">
								<div class="form-group">
									<label>Select Venue:</label>
									<select id="addVenueType" class="form-control select2">
																			
									</select>
								</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Select Logo</label>
								<input type="file" class="filestyle"  name="afile" id="afile" accept="image/*" data-btnClass="btn-primary">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Description</label>
								<textarea type="text" id="venue_decp" name="venue_decp" class="form-control" ></textarea>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Street Address: </label>
								<input type="text" id="venue_count" name="venue_count" class="form-control" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>City: </label>
							
								<input type="text" id="venue_state" name="venue_state" class="form-control" >
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>State: </label>
							
								<input type="text" id="venue_city" name="venue_city" class="form-control" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Country: </label>
								<input type="text" id="venue_street" name="venue_street" class="form-control" >
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Phone: </label>
								<input type="text" id="venue_fb" name="venue_fb" class="form-control" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Email: </label>
								<input type="text" id="venue_twitter" name="venue_twitter" class="form-control" >
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Website: </label>
								<input type="text" id="venue_insta" name="venue_insta" class="form-control" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Facebook: </label>
								<input type="text" id="venue_utube" name="venue_utube" class="form-control" >
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Instagram: </label>
								<input type="text" id="venue_phone" name="venue_phone" class="form-control" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Twitter: </label>
								<input type="text" id="venue_email" name="venue_email" class="form-control" >
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Youtube: </label>
								<input type="text" id="venue_web" name="venue_web" class="form-control" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Untapped: </label>
								<input type="text" id="venue_email" name="venue_email" class="form-control" >
							</div>
						</div>

					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Monday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="mopen" name="mopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="mclose" name="mclose" class="form-control" >
							</div>
						</div>
					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Tuesday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="topen" name="topen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="tclose" name="tclose" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Wednesday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="wedopen" name="wedopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="wedclose" name="wedclose" class="form-control" >
							</div>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Thursday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="thopen" name="thopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="thclose" name="thclose" class="form-control" >
							</div>
						</div>
					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Friday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="friopen" name="friopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="friclose" name="friclose" class="form-control" >
							</div>
						</div>
					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Saturday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="satopen" name="satopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="satclose" name="satclose" class="form-control" >
							</div>
						</div>
					</div>
					
					
					<div class="row">
						
						<div class="col-md-2">
							<div class="form-group">
								<label>Sunday : </label>
								
							</div>
						</div>
					
						<div class="col-md-5">
							<div class="form-group">
								<label>From: </label>
								<input type="time" id="sunopen" name="sunopen" class="form-control" >
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label>To: </label>
								<input type="time" id="sunclose" name="sunclose" class="form-control" >
							</div>
						</div>
					</div>






			</div>
			<div class="modal-footer">
				<button type="button" onclick="saveVenuesImage()" class="btn btn-primary" >Save</button>
				<button type="button" class="btn btn-warning"  data-dismiss="modal" >Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->