			<div id="modal_theme_add_venue" class="modal fade bs-venue-modal-lg"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">

							<h4 class="modal-title" id="myLargeModalLabel">Update Venue Details</h4>
						</div>
						<div class="modal-body">


								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Venue Name</label>
											<input type="text" id="uvenue_name" name="uvenue_name" class="form-control" >
										</div>
									</div>
									<div class="col-md-6">
											<div class="form-group">
												<label>Venue Type:</label>
												<select id="editVenueType" class="form-control select2">
																						
												</select>
											</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Select Logo</label>
											<input type="file" class="filestyle"  name="ufile" id="ufile" accept="image/*" data-btnClass="btn-primary">
											<input type="hidden" id="ufilename" name="ufilename" class="form-control" >
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Description</label>
											<textarea type="text" id="uvenue_decp" name="uvenue_decp" class="form-control" ></textarea>
										</div>
									</div>
								</div>


								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Street Address: </label>
											<input type="text" id="uvenue_street" name="uvenue_street" class="form-control" >
										</div>
									</div>
									<div class="col-md-6">
									<div class="form-group">
											<label>City: </label>
										
											<input type="text" id="uvenue_city" name="uvenue_city" class="form-control" >
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
									
										<div class="form-group">
											<label>State: </label>
										
											<input type="text" id="uvenue_state" name="uvenue_state" class="form-control" >
										</div>
										
									</div>
									<div class="col-md-6">
									<div class="form-group">
											<label>Country: </label>
											<input type="text" id="uvenue_count" name="uvenue_count" class="form-control" >
										</div>
										
									</div>
								</div>
									<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Phone: </label>
											<input type="text" id="uvenue_phone" name="uvenue_phone" class="form-control" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Email: </label>
											<input type="text" id="uvenue_email" name="uvenue_email" class="form-control" >
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
									<div class="form-group">
											<label>Website: </label>
											<input type="text" id="uvenue_web" name="uvenue_web" class="form-control" >
										</div>
									
									</div>
									<div class="col-md-6">
									<div class="form-group">
											<label>Facebook: </label>
											<input type="text" id="uvenue_fb" name="uvenue_fb" class="form-control" >
										</div>
										
									</div>
								</div>


								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Instagram: </label>
											<input type="text" id="uvenue_insta" name="uvenue_insta" class="form-control" >
										</div>
									</div>
									<div class="col-md-6">
									<div class="form-group">
											<label>Twitter: </label>
											<input type="text" id="uvenue_twitter" name="uvenue_twitter" class="form-control" >
										</div>
										
									</div>
								</div>


								

											<div class="row">
												<div class="col-md-6">
												<div class="form-group">
														<label>Youtube: </label>
														<input type="text" id="uvenue_utube" name="uvenue_utube" class="form-control" >
													</div>
													
												</div>
												<div  class="col-md-6">
													<div class ="form-group">
															<label> Untapped: </label>
															<input type="text" id="uvenue_untapped" name="uvenue_untapped" class="form-control">
													</div>
												
												</div>

											</div>	
									







						</div>
						<div class="modal-footer">
							<button type="button" onclick="updatevenueimagedetails()" class="btn btn-primary" >Save</button>
							<button type="button" class="btn btn-warning"  data-dismiss="modal" >Close</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->