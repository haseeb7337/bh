<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Brewery Hours - Event Management </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- Plugins CSS -->
		<link href="../plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="../plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="../plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="../plugins/switchery/switchery.min.css"> 

        <script src="assets/js/modernizr.min.js"></script>
		<script type="text/javascript" src="../js/baseurl.js"></script>

		<script>
		
		
		window.fbAsyncInit = function() {
			FB.init({
			  appId            : '406585349804686',
			  autoLogAppEvents : true,
			  xfbml            : true,
			  version          : 'v2.12'
			});
		  };

		  (function(d, s, id){
			 var js, fjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) {return;}
			 js = d.createElement(s); js.id = id;
			 js.src = "https://connect.facebook.net/en_US/sdk.js";
			 fjs.parentNode.insertBefore(js, fjs);
	    }(document, 'script', 'facebook-jssdk'));
		
	/*	FB.api('/113124472034820', function(response) {
		  console.log(response);
		});*/


		//get all evetns
		function LoadTableData(){
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				//console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				
				//init the table and clear table for new entires
				var t = $('#eventsTable').DataTable();
				t.clear().draw();
				for (var i = 0; i < jsonData.result.length; i++) {
					var counter = jsonData.result[i];
					t.row.add([
						counter.venue_event_title,
						counter.venue_event_descp,
						counter.venue_event_end_date,
						counter.event_type_cat_name,
						"<a href='#'  class='btn btn-sm btn-custom'><i class='mdi mdi-plus'></i></a> "
						
					]).draw(true);
				}
				

			  }
			});

			xhr.open("GET", baseurl+"/api/public/events/get");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "4570bf87-1e10-4488-b58f-f875796d5465");

			xhr.send(data);
		}
		
		//add new event
		var current_venue_id;
		var current_event_type_id;
		function AddNewEvent(){
			//$('.bs-event-modal-lg').modal('show');
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				//console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				//for loading the types
				for (var i = 0; i < jsonData.result.eventypes.length; i++) {
					var counter = jsonData.result.eventypes[i];
					
					var option = document.createElement("option");
					option.text = counter.event_type_cat_name;
					option.value = counter.event_type_cat_id;
					if(i == 0){		
						current_event_type_id = counter.event_type_cat_id; 							//CORRECT THIS
					}
					select = document.getElementById("eventtype");
					select.appendChild(option);
				}
				
				
				//for loading the venues
				for (var i = 0; i < jsonData.result.venues.length; i++) {
					var counter = jsonData.result.venues[i];
					
					var option = document.createElement("option");
					option.text = counter.venue_name;
					option.value = counter.venue_id;
					if(i == 0){
						current_venue_id = counter.venue_id;								//CORRECT THIS
					}
					
					select = document.getElementById("venuenames");
					select.appendChild(option);
				}
				
				
				$('.bs-event-modal-lg').modal('show');
				
				
			  }
			});

			xhr.open("GET", baseurl+"/api/public/events/createdetails");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "e14c7056-b013-472c-96fc-dd58eda56017");
			
			document.getElementById('eventtype').onchange = function(){
				document.getElementById('form_id').action = '/'+this.value;
				current_event_type_id = this.value;
				//alert(this.value);
			}
			
			document.getElementById('venuenames').onchange = function(){
				document.getElementById('form_id').action = '/'+this.value;
				current_venue_id = this.value;
				//alert(this.value);
			}
			xhr.send(data);			
		}
		
		//saving the event
		function saveEvent(){
			//alert(current_event_type_id);
			var event_name = document.getElementById('event_name').value;
			var img_cap = document.getElementById('img_cap').value;
			var event_decp = document.getElementById('event_decp').value;
			var event_start_date = document.getElementById('event_start_date').value;
			var event_end_date = document.getElementById('event_end_date').value;
			var event_s_time = document.getElementById('event_s_time').value;
			var event_e_time = document.getElementById('event_e_time').value;
			
			
			
			var data = JSON.stringify({
			  "venue_event_title": ""+event_name+"",
			  "venue_event_pic": "",
			  "venue_event_pic_cap": ""+img_cap+"",
			  "venue_event_descp": ""+event_decp+"",
			  "venue_event_start_date": ""+event_start_date+"",
			  "venue_event_end_date": ""+event_end_date+"",
			  "venue_event_multi_dates": "NO",
			  "venue_event_start_time": ""+event_s_time+"",
			  "venue_event_end_time": ""+event_e_time+"",
			  "event_type_cat_id": ""+current_event_type_id+"",
			  "venue_id": ""+current_venue_id+"",
			  "is_event_active": "1"
			});
			
			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
				alert("Event has been created");
				location.reload();
			  }
			});
			
			
			xhr.open("POST", baseurl+"/api/public/events/create");
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "36db96e3-cfb6-4803-b170-1b9f9cd4980e");

			xhr.send(data);

			
		}
		
		function CloseModol(){
			/*var dd = new Date();
			var days = ['sunday', 'monday', 'tuesday', 'wednesday', 'yhursday', 'friday', 'saturday'];
			var d = new Date(dd);
			var dayName = days[d.getDay()];
			alert(dayName+"_close");*/
			location.reload();
		}
		</script>
				<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore…
				})(window,document,'script','dataLayer','GTM-N5ZZR49');
		</script>
		<!-- End Google Tag Manager -->
      
    </head>


    <body onload="LoadTableData()">
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5ZZR49"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                              <span>
                                <img src="assets/images/brewery_hours.png" alt="" height="65">
                            </span>
                           <i>
                                <img src="assets/images/brewery_hours.png" alt="" height="68">
                            </i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                       <h5><a href="#">Jason Bass</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
                        <?php include('menu.php');  ?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>


                            

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">
						 <div class="row">
							<div class="col-lg-12">
								<div class="card-box">
								<h4 class="header-title mb-3">Manage your Events</h4>
									<div class="table-responsive">
										<table id="eventsTable"  class="table table-hover table-centered m-0">
											<thead>
                                            <tr>
                                                <th>Event Title</th>
                                                <th>Event Description</th>
                                                <th>Dates</th>
                                                <th>Event Type</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
											
										</table>
									</div>
									   <!-- Signup modal content -->
										<?php include('includes/add_event_modol.php'); ?>

                                    <div class="button-list">
                                         <button onclick="AddNewEvent()" type="button" class="btn btn-info waves-effect waves-light" >Add Event</button>
                                    </div>
								</div>
							</div>
						 </div>
                    </div> <!-- container -->

                </div> <!-- content -->

               
                   <?php include ('footer.php'); ?>
                

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		
		
		
		<script src="../plugins/switchery/switchery.min.js"></script>
        <script src="../plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
        <script src="../plugins/select2/js/select2.min.js" type="text/javascript"></script>
        <script src="../plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>
        <script src="../plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <script src="../plugins/bootstrap-maxlength/bootstrap-maxlength.js" type="text/javascript"></script>

        <script type="text/javascript" src="../plugins/autocomplete/jquery.mockjax.js"></script>
        <script type="text/javascript" src="../plugins/autocomplete/jquery.autocomplete.min.js"></script>
        <script type="text/javascript" src="../plugins/autocomplete/countries.js"></script>
        <script type="text/javascript" src="assets/pages/jquery.autocomplete.init.js"></script>
		

       <!-- Required datatable js -->
       <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="../plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="../plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
		
		 <!-- Modal-Effect -->
        <script src="../plugins/custombox/js/custombox.min.js"></script>
        <script src="../plugins/custombox/js/legacy.min.js"></script>
		
		        <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>


    </body>
</html>