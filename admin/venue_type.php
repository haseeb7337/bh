<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Highdmin - Responsive Bootstrap 4 Admin Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>
		<script type="text/javascript" src="../js/baseurl.js"></script>
        <script>
        function addVenue(){
            $('.bs-example-modal-lg').modal('show');	
        }


        function LoadTableData(){
            var data = null;

            var xhr = new XMLHttpRequest();
            xhr.withCredentials = false;

            xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                console.log(this.responseText);
                var jsonData = JSON.parse(this.responseText);
				
				//init the table and clear table for new entires
				var t = $('#venuestypetable').DataTable();
				t.clear().draw();
				//for loop to get all the result to table
				for (var i = 0; i < jsonData.result.length; i++) {
					//getting each node in array in a seperate varibable
					var counter = jsonData.result[i];
					
					//adding to the table
					t.row.add([
						counter.venue_type_name,
						counter.venue_type_created,
						"<a href='#' onclick='checkVenue("+counter.venue_type_id+" ,\""+counter.venue_type_name+"\")' class='btn btn-sm btn-custom'><i class='mdi mdi-plus'></i></a> <a href='#' class='btn btn-sm btn-danger'><i class='mdi mdi-minus'></i></a>"
							//check above line
					]).draw(true);
				}//for loop ends over here
            }
            });

            xhr.open("GET", baseurl+"/api/public/venues/types");
            xhr.setRequestHeader("Cache-Control", "no-cache");
            xhr.setRequestHeader("Postman-Token", "0201ef94-e628-4eac-82a8-78dc57ceb79c");

            xhr.send(data);
        }
		
		// check this function......!!
		function checkVenue(venue_type_id , venue_type_name){
			//alert(venue_type_id);
			//alert(venue_type_name);
			var current = document.getElementById('editVenueType');
			current.value = venue_type_name;
			current = null;
			
            $('.bs-edit-modal-lg').modal('show');	
        }
		
		
		//check this function.....!!
		function saveeditVenue(){
			
		}
        </script>
     
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore…
				})(window,document,'script','dataLayer','GTM-N5ZZR49');
		</script>
		<!-- End Google Tag Manager -->
        

    </head>


     <body onload="LoadTableData()">
		 <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5ZZR49"
	     height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                            <span>
                                <img src="assets/images/brewery_hours.png" alt="" height="65">
                            </span>
                            <i>
                                <img src="assets/images/brewery_hours.png" alt="" height="68">
								</i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                        <h5><a href="#">Maxine Kennedy</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
                    <?php include ('menu.php'); ?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>


                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title">Venu Types </h4>

                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                     <div class="row">
							<div class="col-lg-12">
								<div class="card-box">
							
									<div class="table-responsive">
										<table id="venuestypetable"  class="table table-hover table-centered m-0">
											<thead>
                                            <tr>
                                                <th>Venue Type</th>
                                                <th>Created On</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
											
										</table>
									</div>
									   <!-- Signup modal content -->
										<?php include('includes/add_venue_modol.php'); ?>
										<?php include('includes/edit_venue_type_modol.php'); ?>

                                    <div class="button-list">
                                         <button onclick="addVenue()" type="button" class="btn btn-info waves-effect waves-light" >Add Venue Type</button>
                                    </div>
								</div>
							</div>
						 </div>

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php include('footer.php');?>
            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->
        



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>

       <!-- Required datatable js -->
       <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="../plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="../plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
		
		 <!-- Modal-Effect -->
        <script src="../plugins/custombox/js/custombox.min.js"></script>
        <script src="../plugins/custombox/js/legacy.min.js"></script>
		
		        <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

    </body>
</html>


