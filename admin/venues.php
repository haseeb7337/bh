<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Brewery Hours - Venue Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
		
			<!-- Plugins CSS -->
		<link href="../plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
		<link href="../plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
		<link href="../plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../plugins/switchery/switchery.min.css">
			
		 <!-- DataTables -->
        <link href="../plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="../plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
		
		<!-- Custom box css -->
        <link href="../plugins/custombox/css/custombox.min.css" rel="stylesheet">

		
        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>
		<script type="text/javascript" src="../js/baseurl.js"></script> 
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-N5ZZR49');</script>
		<!-- End Google Tag Manager -->
		
		
		<script>
		
		function LoadTableData()
		{
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				//console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				
				//init the table and clear table for new entires
				var t = $('#venuestable').DataTable();
				t.clear().draw();
				//for loop to get all the result to table
				for (var i = 0; i < jsonData.result.length; i++) {
					//getting each node in array in a seperate varibable
					var counter = jsonData.result[i];
					
					//adding to the table
					//src="'+baseurl+'/api/app/Controllers/uploads/'+counter.venue_image+'"
					t.row.add([
						'<img src="'+baseurl+'/api/app/Controllers/uploads/'+counter.venue_image+'"  alt="contact-img" title="contact-img" class="rounded-circle thumb-sm">',
						//"<img src='"+baseurl+"'/api/app/Controllers/uploads/'"+counter.venue_image+"' alt='contact-img' title='contact-img' class='rounded-circle thumb-sm'/>",
						counter.venue_name,
						counter.venue_created_date,
						counter.venue_type_name,
						counter.claimed_by,
						"<img onclick='getTimes("+counter.venue_id+")' src='assets/images/manag_hours.png' alt='contact-img' title='contact-img' height='30' width='30' thumb-sm'/> <a href='#' onclick='updateVenue("+counter.venue_id+")' class='btn btn-sm btn-custom'><i class='mdi mdi-plus'></i></a> <a href='#' onclick='delete_venue("+counter.venue_id+")' class='btn btn-sm btn-danger'><i class='mdi mdi-minus'></i></a> "
						
					]).draw(true);
				}//for loop ends over here
			  }
			});

			xhr.open("GET", baseurl+"/api/public/venues/get");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "70ec44e7-56bf-41f5-90cf-1aa8f60641be");

			xhr.send(data);
		}
		function delete_venue(venue_id){
		
				var data = JSON.stringify({
				  "venue_id": ""+venue_id+""
				});

				var xhr = new XMLHttpRequest();
				xhr.withCredentials = true;

				xhr.addEventListener("readystatechange", function () {
				  if (this.readyState === 4) {
					//console.log(this.responseText);
					alert("Your venue has been deleted")
					location.reload();
				  }
				});

				xhr.open("DELETE", baseurl+"/api/public/venues/del");
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.setRequestHeader("Cache-Control", "no-cache");
				xhr.setRequestHeader("Postman-Token", "73666bbb-fda7-4901-8acc-69d0058a8009");

				xhr.send(data);
		}
		
		function check(venue_id){
			alert("FINE----"+ venue_id);
		}
		
		//getting the venues times to update
		var insideVenueId;
		function getTimes(venue_id){
			var data = null;
			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;
			insideVenueId = venue_id;			//storing the venue id in here
			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				//console.log(this.responseText);
				
				
				var jsonData = JSON.parse(this.responseText);
				for (var i = 0; i < jsonData.result.length; i++) {
					
					//console.log(counter.dua_arabic);
				}
				
				var counter = jsonData.result[0];
				//console.log(counter.tuesday_close); 
				var current = document.getElementById("emopen");
				current.value = counter.monday_close;
				current = null;
				var current = document.getElementById("emclose");
				current.value = counter.monday_close;
				current = null;
				
				
				var current = document.getElementById("etopen");
				current.value = counter.tuesday_open;
				current = null;
				var current = document.getElementById("etclose");
				current.value = counter.tuesday_close;
				current = null;
				
				
				var current = document.getElementById("ewedopen");
				current.value = counter.wednesday_open;
				current = null;
				var current = document.getElementById("ewedclose");
				current.value = counter.wednesday_close;
				current = null;
				
				
				var current = document.getElementById("ethopen");
				current.value = counter.thursday_open;
				current = null;
				var current = document.getElementById("ethclose");
				current.value = counter.thursday_close;
				current = null;
				
				
				var current = document.getElementById("efriopen");
				current.value = counter.friday_open;
				current = null;
				var current = document.getElementById("efriclose");
				current.value = counter.friday_close;
				current = null;
				
				
				var current = document.getElementById("esatopen");
				current.value = counter.saturday_open;
				current = null;
				var current = document.getElementById("esatclose");
				current.value = counter.saturday_close;
				current = null;
				
				
				var current = document.getElementById("esunopen");
				current.value = counter.sunday_open;
				current = null;
				var current = document.getElementById("esunclose");
				current.value = counter.sunday_close;
				current = null;
				
				
				$('.bs-time-modal-lg').modal('show');	
			  }
			});

			xhr.open("GET", baseurl+"/api/public//venues/times/admin/"+insideVenueId);
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "11f06570-8d25-4637-b30f-fb611e23416c");

			xhr.send(data);
		}
		
		//updating the times method
		function updateTimes(){
			
			
			var emopen = document.getElementById('emopen').value;
			var emclose = document.getElementById('emclose').value;
			
			var etopen = document.getElementById('etopen').value;
			var etclose = document.getElementById('etclose').value;
			
			var ewedopen = document.getElementById('ewedopen').value;
			var ewedclose = document.getElementById('ewedclose').value;
			
			var ethopen = document.getElementById('ethopen').value;
			var ethclose = document.getElementById('ethclose').value;
			
			var efriopen = document.getElementById('efriopen').value;
			var efriclose = document.getElementById('efriclose').value;
			
			var esatopen = document.getElementById('esatopen').value;
			var esatclose = document.getElementById('esatclose').value;
			
			var esunopen = document.getElementById('esunopen').value;
			var esunclose = document.getElementById('esunclose').value;
			
			
			var data = JSON.stringify({
			  "mopen": ""+emopen+"",
			  "mclose": ""+emclose+"",
			  "topen": ""+etopen+"",
			  "tclose": ""+etclose+"",
			  "wedopen": ""+ewedopen+"",
			  "wedclose": ""+ewedclose+"",
			  "thopen": ""+ethopen+"",
			  "thclose": ""+ethclose+"",
			  "friopen": ""+efriopen+"",
			  "friclose": ""+efriclose+"",
			  "satopen": ""+esatopen+"",
			  "satclose": ""+esatclose+"",
			  "sunopen": ""+esunopen+"",
			  "sunclose": ""+esunclose+""
			});

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
				alert("Timings Have been updated");
				$('.bs-time-modal-lg').modal('hide');
			  }
			});

			xhr.open("POST", baseurl+"/api/public/venues/update/time/"+insideVenueId);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "65bffa6a-ce76-4683-a1bb-1a82d77d48fc");

			xhr.send(data);
		}
		var update_id;//this is for updating venue type 
		//getting a single venue to update
		function updateVenue(venue_id){
			
			insideVenueId = venue_id;
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				
				for (var i = 0; i < jsonData.result.types.length; i++) {
					var counter = jsonData.result.types[i];
					
					var option = document.createElement("option");
					option.text = counter.venue_type_name;
					option.value = counter.venue_type_id;
					if(i == 0){
						update_id = counter.venue_type_id;
					}
					
					select = document.getElementById("editVenueType");
					select.appendChild(option);
				}
				document.getElementById('editVenueType').onchange = function(){
					document.getElementById('editVenueType').action = '/'+this.value;
					update_id = this.value;
					//alert(this.value);
				}
				var counter = jsonData.result.details[0];
				
				//console.log(counter.venue_name); 
				
				
				
				var current = document.getElementById('uvenue_name');
				current.value = counter.venue_name;
				current = null;
				
				var current = document.getElementById('ufilename');
				current.value = counter.venue_image;
				current = null;
				
				
				var current = document.getElementById('uvenue_decp');
				current.value = counter.venue_desc;
				current = null;
				
				var current = document.getElementById('uvenue_city');
				current.value = counter.venue_city;
				current = null;
				var current = document.getElementById('uvenue_state');
				current.value = counter.venue_state;
				current = null;
				
				var current = document.getElementById('uvenue_street');
				current.value = counter.venue_street;
				current = null;
				
				var current = document.getElementById('uvenue_phone');
				current.value = counter.venue_phone;
				current = null;
				
				var current = document.getElementById('uvenue_fb');
				current.value = counter.venue_facebook;
				current = null;
				
				var current = document.getElementById('uvenue_twitter');
				current.value = counter.venue_twitter;
				current = null;
				
				var current = document.getElementById('uvenue_insta');
				current.value = counter.venue_insta;
				current = null;
				
				var current = document.getElementById('uvenue_utube');
				current.value = counter.venue_youtube;
				current = null;
				
				var current = document.getElementById('uvenue_web');
				current.value = counter.venue_website;
				current = null;
				
				var current = document.getElementById('uvenue_email');
				current.value = counter.venue_email;
				current = null;
				
				$('.bs-venue-modal-lg').modal('show');	
				
			  }
			});

			xhr.open("GET", baseurl+"/api/public//venues/get/"+insideVenueId);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "18298e1d-d408-402e-8570-ae104ed62320");

			xhr.send(data);
		}
		
		//saving the updated information for brwies
		function saveVenuesDetails(){
			
			var venue_name = document.getElementById('uvenue_name').value;
			var venue_decp = document.getElementById('uvenue_decp').value;
			var venue_city = document.getElementById('uvenue_city').value;
			var venue_state = document.getElementById('uvenue_state').value;
			var venue_street = document.getElementById('uvenue_street').value;
			var venue_phone = document.getElementById('uvenue_phone').value;
			var venue_fb = document.getElementById('uvenue_fb').value;
			var venue_twitter = document.getElementById('uvenue_twitter').value;
			var venue_insta = document.getElementById('uvenue_insta').value;
			var venue_utube = document.getElementById('uvenue_utube').value;
			var venue_web = document.getElementById('uvenue_web').value;
			var venue_email = document.getElementById('uvenue_email').value;
			var data = JSON.stringify({
			  "venue_name": ""+venue_name+"",
			  "venue_desc": ""+venue_decp+"",
			  "venue_street": ""+venue_street+"",
			  "venue_state": ""+venue_state+"",
			  "venue_city": ""+venue_city+"",
			  "venue_phone": ""+venue_phone+"",
			  "venue_twitter": ""+venue_twitter+"",
			  "venue_insta": ""+venue_insta+"",
			  "venue_facebook": ""+venue_fb+"",
			  "venue_youtube": ""+venue_utube+"",
			  "venue_website": ""+venue_web+"",
			  "venue_email": ""+venue_email+""
			});

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				//console.log(this.responseText);
				alert("Venue Details have been updated.");
				location.reload();
				$('.bs-venue-modal-lg').modal('hide');	
				
			  }
			});

			xhr.open("POST", baseurl+"/api/public/venues/update/information/"+insideVenueId);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "ce2e8587-d870-4df3-8cea-af3ee391afcc");

			xhr.send(data);
		}
		
		
		
		//adding a venue
		var status_id;
		function addVenue(){
			var data = null;

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = false

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
				var jsonData = JSON.parse(this.responseText);
				for (var i = 0; i < jsonData.result.length; i++) {
					var counter = jsonData.result[i];
					
					var option = document.createElement("option");
					option.text = counter.venue_type_name;
					option.value = counter.venue_type_id;
					if(i == 0){
						status_id = counter.venue_type_id;
					}
					
					select = document.getElementById("addVenueType");
					select.appendChild(option);
				}
				//alert(status_id);
				$('.bs-example-modal-lg').modal('show');
				
			  }
			});

			xhr.open("GET", baseurl+"/api/public/venues/types");
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "e9a3bc50-928b-4457-b7ee-a57624b2be8e");
			
			document.getElementById('addVenueType').onchange = function(){
				document.getElementById('addVenueType').action = '/'+this.value;
				status_id = this.value;
				//alert(this.value);
			}

			xhr.send(data);
		}
		
		function saveVenue(){
			var venue_name = document.getElementById('venue_name').value;
			var venue_decp = document.getElementById('venue_decp').value;
			var venue_city = document.getElementById('venue_city').value;
			var venue_state = document.getElementById('venue_state').value;
			var venue_street = document.getElementById('venue_street').value;
			var venue_phone = document.getElementById('venue_phone').value;
			var venue_fb = document.getElementById('venue_fb').value;
			var venue_twitter = document.getElementById('venue_twitter').value;
			var venue_insta = document.getElementById('venue_insta').value;
			var venue_utube = document.getElementById('venue_utube').value;
			var venue_web = document.getElementById('venue_web').value;
			var venue_email = document.getElementById('venue_email').value;
			
			var mopen = document.getElementById('mopen').value;
			var mclose = document.getElementById('mclose').value;
			
			var topen = document.getElementById('topen').value;
			var tclose = document.getElementById('tclose').value;
			
			var wedopen = document.getElementById('wedopen').value;
			var wedclose = document.getElementById('wedclose').value;
			
			var thopen = document.getElementById('thopen').value;
			var thclose = document.getElementById('thclose').value;
			
			var friopen = document.getElementById('friopen').value;
			var friclose = document.getElementById('friclose').value;
			
			var satopen = document.getElementById('satopen').value;
			var satclose = document.getElementById('satclose').value;
			
			var sunopen = document.getElementById('sunopen').value;
			var sunclose = document.getElementById('sunclose').value;
			
			
			var message;
			if(!venue_name ){
				message = "Plese provide your Venue Name";
				//alert("Please enter all information");
			}
			if(!venue_decp ){
				message = "Plese provide Venue Description";
				//alert("Please enter all information");
			}
			if(!venue_city ){
				message = "Plese provide Venue City";
				//alert("Please enter all information");
			}
			if(!venue_state ){
				message = "Plese provide State";
				//alert("Please enter all information");
			}
			
			
			if(!venue_fb ){
				message = "Plese provide your Facebook Page Link";
				//alert("Please enter all information");
			}
			
			if(!status_id){
				message = message+ "\nPlease select a category";
			}
			
			if(message){
				alert(message);
			}
			else{
				//alert("perfect");
				var data = JSON.stringify({
				  "venue_name": ""+venue_name+"",
				  "venue_desc": ""+venue_decp+"",
				  "venue_type_id":  ""+status_id+"",
				  "venue_street": ""+venue_street+"",
				  "venue_state": ""+venue_state+"",
				  "venue_city": ""+venue_city+"",
				  "venue_phone": ""+venue_city+"",
				  "venue_twitter": ""+venue_twitter+"",
				  "venue_insta": ""+venue_insta+"",
				  "venue_facebook":""+venue_fb+"",
				  "venue_youtube": ""+venue_utube+"",
				  "venue_website": ""+venue_web+"",
				  "venue_email": ""+venue_email+"",
				  "mopen": ""+mopen+"",
				  "mclose": ""+mclose+"",
				  "topen": ""+topen+"",
				  "tclose": ""+tclose+"",
				  "wedopen": ""+wedopen+"",
				  "wedclose":""+wedclose+"",
				  "thopen": ""+thopen+"",
				  "thclose": ""+thclose+"",
				  "friopen": ""+friopen+"",
				  "friclose": ""+friclose+"",
				  "satopen": ""+satopen+"",
				  "satclose": ""+satclose+"",
				  "sunopen": ""+sunopen+"",
				  "sunclose": ""+sunclose+""
				});

				var xhr = new XMLHttpRequest();
				xhr.withCredentials = false

				xhr.addEventListener("readystatechange", function () {
				  if (this.readyState === 4) {
					console.log(this.responseText);
					var select = document.getElementById("addVenueType");
					var length = select.options.length;
					for (i = 0; i < length; i++) {
					  select.options[i] = null;
					}
					alert("Venue has be Created");
					$('.bs-example-modal-lg').modal('hide');
					window.location.reload();
				  }
				});

				xhr.open("POST", baseurl+"/api/public/venues/create");
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.setRequestHeader("Cache-Control", "no-cache");
				xhr.setRequestHeader("Postman-Token", "cacc51fe-1c14-4015-988c-8ba3713d4f20");

				xhr.send(data);
			}
		}
		
		/*function saveVenuesImage(){
			/*var venue_name = document.getElementById('venue_name').value;
			var venue_decp = document.getElementById('venue_decp').value;
			var venue_city = document.getElementById('venue_city').value;
			var venue_state = document.getElementById('venue_state').value;
			var venue_street = document.getElementById('venue_street').value;
			var venue_phone = document.getElementById('venue_phone').value;
			var venue_fb = document.getElementById('venue_fb').value;
			var venue_twitter = document.getElementById('venue_twitter').value;
			var venue_insta = document.getElementById('venue_insta').value;
			var venue_utube = document.getElementById('venue_utube').value;
			var venue_web = document.getElementById('venue_web').value;
			var venue_email = document.getElementById('venue_email').value;
			
			var mopen = document.getElementById('mopen').value;
			var mclose = document.getElementById('mclose').value;
			
			var topen = document.getElementById('topen').value;
			var tclose = document.getElementById('tclose').value;
			
			var wedopen = document.getElementById('wedopen').value;
			var wedclose = document.getElementById('wedclose').value;
			
			var thopen = document.getElementById('thopen').value;
			var thclose = document.getElementById('thclose').value;
			
			var friopen = document.getElementById('friopen').value;
			var friclose = document.getElementById('friclose').value;
			
			var satopen = document.getElementById('satopen').value;
			var satclose = document.getElementById('satclose').value;
			
			var sunopen = document.getElementById('sunopen').value;
			var sunclose = document.getElementById('sunclose').value;
			
			
			var message;
			if(!venue_name ){
				message = "Plese provide your Venue Name";
				//alert("Please enter all information");
			}
			if(!venue_decp ){
				message = "Plese provide Venue Description";
				//alert("Please enter all information");
			}
			if(!venue_city ){
				message = "Plese provide Venue City";
				//alert("Please enter all information");
			}
			if(!venue_state ){
				message = "Plese provide State";
				//alert("Please enter all information");
			}
			
			
			if(!venue_fb ){
				message = "Plese provide your Facebook Page Link";
				//alert("Please enter all information");
			}
			
			if(!status_id){
				message = message+ "\nPlease select a category";
			}
			
			if(message){
				alert(message);
			}
			
			var uploads = document.getElementById('afile').value;
			
			//var file = this.files[0];
			
			alert(uploads);
			
			//form entries go here
			var data = new FormData();
			
			data.append("afile", ""+uploads+"");
			

			var xhr = new XMLHttpRequest();
			xhr.withCredentials = true;

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				console.log(this.responseText);
			  }
			});

			xhr.open("POST", "http://localhost:8080/brweyhour/api/public/venues/create" , true);
			xhr.setRequestHeader("Content-type","multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2));
			xhr.setRequestHeader("Cache-Control", "no-cache");
			xhr.setRequestHeader("Postman-Token", "d1aa748e-e09f-48ca-8932-56a69f1242fb");

			xhr.send(data);
		}*/
		
		function updatevenueimagedetails(){
			var venue_name = document.getElementById('uvenue_name').value;
			var venue_image = document.getElementById('ufilename').value;
			var venue_decp = document.getElementById('uvenue_decp').value;
			var venue_city = document.getElementById('uvenue_city').value;
			var venue_state = document.getElementById('uvenue_state').value;
			var venue_street = document.getElementById('uvenue_street').value;
			var venue_phone = document.getElementById('uvenue_phone').value;
			var venue_fb = document.getElementById('uvenue_fb').value;
			var venue_twitter = document.getElementById('uvenue_twitter').value;
			var venue_insta = document.getElementById('uvenue_insta').value;
			var venue_utube = document.getElementById('uvenue_utube').value;
			var venue_web = document.getElementById('uvenue_web').value;
			var venue_email = document.getElementById('uvenue_email').value;
			
			var file = ufile.files[0];
			//alert (file);
			var data = new FormData();
			//	data.append("venue_name", ""+venue_name+"");
			data.append("ufile", file);
			data.append("venue_name", ""+venue_name+"");
			data.append("venue_desc", ""+venue_decp+"");
			data.append("venue_type_id", ""+update_id+"");
			data.append("venue_street", ""+venue_street+"");
			data.append("venue_state", ""+venue_state+"");
			data.append("venue_city", ""+venue_city+"");
			data.append("venue_phone", ""+venue_phone+"");
			data.append("venue_twitter", ""+venue_twitter+"");
			data.append("venue_insta", ""+venue_insta+"");
			data.append("venue_facebook", ""+venue_fb+"");
			data.append("venue_youtube", ""+venue_utube+"");
			data.append("venue_website", ""+venue_web+"");
			data.append("venue_email", ""+venue_email+"");
			data.append("venue_image", ""+venue_image+"");
			
			  var xhr = new XMLHttpRequest();
			  xhr.withCredentials = false;
			  
			  
			  xhr.addEventListener("readystatechange", function () {
				  if (this.readyState === 4) {
					  console.log(this.responseText);
					  alert("Venue details have been Updated");
					  location.reload();
				  }
			  });
			  
			  
			  xhr.open('POST', baseurl+'/api/public/venues/update/information/'+insideVenueId, true);
			 //xhr.open('POST', 'http://localhost:8080/brweyhour/api/public/venues/create', true);
			  xhr.send(data);
		  
		}
		function saveVenuesImage() {
			
			var venue_name = document.getElementById('venue_name').value;
			var venue_decp = document.getElementById('venue_decp').value;
			var venue_city = document.getElementById('venue_city').value;
			var venue_state = document.getElementById('venue_state').value;
			var venue_street = document.getElementById('venue_street').value;
			var venue_phone = document.getElementById('venue_phone').value;
			var venue_fb = document.getElementById('venue_fb').value;
			var venue_twitter = document.getElementById('venue_twitter').value;
			var venue_insta = document.getElementById('venue_insta').value;
			var venue_utube = document.getElementById('venue_utube').value;
			var venue_web = document.getElementById('venue_web').value;
			var venue_email = document.getElementById('venue_email').value;
			
			var mopen = document.getElementById('mopen').value;
			var mclose = document.getElementById('mclose').value;
			
			var topen = document.getElementById('topen').value;
			var tclose = document.getElementById('tclose').value;
			
			var wedopen = document.getElementById('wedopen').value;
			var wedclose = document.getElementById('wedclose').value;
			
			var thopen = document.getElementById('thopen').value;
			var thclose = document.getElementById('thclose').value;
			
			var friopen = document.getElementById('friopen').value;
			var friclose = document.getElementById('friclose').value;
			
			var satopen = document.getElementById('satopen').value;
			var satclose = document.getElementById('satclose').value;
			
			var sunopen = document.getElementById('sunopen').value;
			var sunclose = document.getElementById('sunclose').value;
			
			
			/*var message;
			if(!venue_name ){
				message = "Plese provide your Venue Name";
				//alert("Please enter all information");
			}
			if(!venue_decp ){
				message = "Plese provide Venue Description";
				//alert("Please enter all information");
			}
			if(!venue_city ){
				message = "Plese provide Venue City";
				//alert("Please enter all information");
			}
			if(!venue_state ){
				message = "Plese provide State";
				//alert("Please enter all information");
			}
			
			
			if(!venue_fb ){
				message = "Plese provide your Facebook Page Link";
				//alert("Please enter all information");
			}
			
			if(!status_id){
				message = message+ "\nPlease select a category";
			}
			
			if(message){
				alert(message);
			}*/
	
			
			
			var file = afile.files[0];
			var data = new FormData();
			data.append("afile", file);
			// These extra params aren't necessary but show that you can include other data.
			//data.append("username", "Groucho");
			//data.append("accountnum", 123456);
			data.append("venue_name", ""+venue_name+"");
			data.append("venue_desc", ""+venue_decp+"");
			data.append("venue_type_id", ""+status_id+"");
			data.append("venue_street", ""+venue_street+"");
			data.append("venue_state", ""+venue_state+"");
			data.append("venue_city", ""+venue_city+"");
			data.append("venue_phone", ""+venue_phone+"");
			data.append("venue_twitter", ""+venue_twitter+"");
			data.append("venue_insta", ""+venue_insta+"");
			data.append("venue_facebook", ""+venue_fb+"");
			data.append("venue_youtube", ""+venue_utube+"");
			data.append("venue_website", ""+venue_web+"");
			data.append("venue_email", ""+venue_email+"");
			data.append("mopen", ""+mopen+"");
			data.append("mclose", ""+mclose+"");
			data.append("topen", ""+topen+"");
			data.append("tclose", ""+tclose+"");
			data.append("wedopen", ""+wedopen+"");
			data.append("wedclose", ""+wedclose+"");
			data.append("thopen", ""+thopen+"");
			data.append("thclose", ""+thclose+"");
			data.append("friopen", ""+friopen+"");
			data.append("friclose", ""+friclose+"");
			data.append("satopen", ""+satopen+"");
			data.append("satclose", ""+satclose+"");
			data.append("sunopen", ""+sunopen+"");
			data.append("sunclose", ""+sunclose+"");
		  
		  
		  /*******************************************************************/

		  var xhr = new XMLHttpRequest();
		  xhr.withCredentials = false;
		  
		  
		  xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4) {
				  console.log(this.responseText);
				  alert("A new Venue has been created.");
				  location.reload();
			  }
		  });
		  
		  
		  xhr.open('POST', baseurl+'/api/public/venues/create', true);
		 //xhr.open('POST', 'http://localhost:8080/brweyhour/api/public/venues/create', true);
		  xhr.send(data);
		  
		  /*xhr.upload.onprogress = function(e) {
			if (e.lengthComputable) {
			  var percentComplete = (e.loaded / e.total) * 100;
			  console.log(percentComplete + '% uploaded');
			}
		  };*/

		 /* xhr.onload = function() {
			if (this.status == 200) {
			  //var resp = JSON.parse(this.response);

			  console.log(this.response);

			  //var image = document.createElement('img');
			  //image.src = resp.dataUrl;
			  //document.body.appendChild(image);
			};
		  };*/

		  
		}
		</script>
	
				 <!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore…
				})(window,document,'script','dataLayer','GTM-N5ZZR49');
				</script>
			<!-- End Google Tag Manager -->
    </head>


    <body onload="LoadTableData()">
	
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5ZZR49"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo">
                            <span>
                                <img src="assets/images/brewery_hours.png" alt="" height="65">
                            </span>
                            <i>
                                <img src="assets/images/brewery_hours.png" alt="" height="68">
								</i>
                        </a>
                    </div>

                    <!-- User box -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div>
                       <h5><a href="#">Jason Bass</a> </h5>
                        <p class="text-muted">Admin Head</p>
                    </div>

                    <!--- Sidemenu -->
					<?php include('menu.php')?>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="hide-phone app-search">
                                <form>
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </form>
                            </li>

                            

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title">View Venues </h4>
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->



                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">
						 <div class="row">
							<div class="col-lg-12">
								<div class="card-box">
									<h4 class="header-title mb-3">Manage your Venues</h4>
									<div class="table-responsive">
										<table id="venuestable"  class="table table-hover table-centered m-0">
											<thead>
                                            <tr>
                                                <th>Profile Image</th>
                                                <th>Name</th>
                                                <th>Registered Date</th>
                                                <th>Venue Type</th>
                                                <th>Claimed by</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
											
										</table>
									</div>
									   <!-- Signup modal content -->
										<?php include('includes/add_venue.php'); ?>
										<?php include('includes/hours_manage.php'); ?>
										<?php include('includes/update_venue.php'); ?>

                                    <div class="button-list">
                                         <button onclick="addVenue()" type="button" class="btn btn-info waves-effect waves-light" >Add Venue</button>
                                    </div>
								</div>
							</div>
						 </div>
                    </div> <!-- container -->

                </div> <!-- content -->

                <?php include('footer.php');?>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
		<script src="../plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
		<script src="../plugins/bootstrap-select/js/bootstrap-select.js" type="text/javascript"></script>
		
		<!-- Required datatable js -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatables/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>

        <!-- Key Tables -->
        <script src="../plugins/datatables/dataTables.keyTable.min.js"></script>

        <!-- Responsive examples -->
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- Selection table -->
        <script src="../plugins/datatables/dataTables.select.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
		
		 <!-- Modal-Effect -->
        <script src="../plugins/custombox/js/custombox.min.js"></script>
        <script src="../plugins/custombox/js/legacy.min.js"></script>
		
		        <script type="text/javascript">
            $(document).ready(function() {

                // Default Datatable
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: false,
                    buttons: ['copy', 'excel', 'pdf']
                });

                // Key Tables

                $('#key-table').DataTable({
                    keys: true
                });

                // Responsive Datatable
                $('#responsive-datatable').DataTable();

                // Multi Selection Datatable
                $('#selection-datatable').DataTable({
                    select: {
                        style: 'multi'
                    }
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

    </body>
</html>