
$(document).ready(function(){
    $(window).scroll(function(){
        var scrollTop = 50
        if($(window).scrollTop() >= scrollTop){
            $('header').addClass('headerFix');
			$('body').addClass('topgap');
			$(".gotop").addClass("gotop-show")
        }
        if($(window).scrollTop() < scrollTop){
            $('header').removeClass('headerFix');
			$('body').removeClass('topgap');
			$(".gotop").removeClass("gotop-show")
        }
    })
})

$(".gotop").click(function(){
	$('html, body').animate({scrollTop: 0},1000);	
})


