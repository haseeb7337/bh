<?php

//start session
session_start();

//load dependies
require __DIR__ . '/../vendor/autoload.php';



//inti the slim framework
$app = new \Slim\App([

    'settings' =>[
        'displayErrorDetails' => true,
    ]
]);


//setting up the containers for the views any of those are used in here
$container = $app->getContainer();
$container['view'] = function($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../resources/views' , [
        'cache' => false,
    ]);

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));
    return $view;
};

//calling the home controller class
$container['HomeController'] = function($container){
    return new \App\Controllers\HomeController($container);
};


//calling the User Controller Class
$container['UsersController'] = function($container){
    return new \App\Controllers\UsersController($container);
};

//calling the User Controller Class
$container['CatController'] = function($container){
    return new \App\Controllers\CatController($container);
};

//calling the wallpaer controller
//calling the User Controller Class
$container['WallpaperController'] = function($container){
    return new \App\Controllers\WallpaperController($container);
};


$container['VenuesController'] = function($container){
    return new \App\Controllers\VenuesController($container);
};


$container['EventsController'] = function($container){
    return new \App\Controllers\EventsController($container);
};


//adding the routues file in here.. 
require __DIR__ . '/../app/routes.php';
?>