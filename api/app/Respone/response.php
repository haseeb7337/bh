<?php


function error($response)
{
    $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
    return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));        
}

function verifyApiKey($request)
{
    if ($request->hasHeader('API')) {
        $headerValueString = $request->getHeaderLine('API');
        if($headerValueString == "12345"){
            return true;
        }else{
            return false;
        }
    }
    else{
        return false;
    }
}

?>