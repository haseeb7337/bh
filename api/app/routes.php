<?php


$app->get('/home' , 'HomeController:index');
$app->post('/upload' , 'HomeController:uploadFile');

//calls related to user actions
$app->get('/users' , 'UsersController:ListAllUsers');
$app->post('/users/login' , 'UsersController:UserLogin');
$app->post('/users/registration' , 'UsersController:UserRegistration');
$app->put('/users/status/{userid}/{usrstatus}', 'UsersController:BlockUser');

//cateogries relation actions
$app->post('/categories/create' , 'CatController:CreateCate');
$app->delete('/categories/delete/{catid}' , 'CatController:DeleteCat');
$app->get('/categories/get/{status}' , 'CatController:GetApprovedCat');
$app->patch('/categories/status/{catid}/{status}' , 'CatController:ApproveDisCat');

//wallpaper api calls
$app->post('/wallpapers/add' , 'WallpaperController:AddWallpaper');
$app->get('/wallpapers/get/{catid}/{status}' , 'WallpaperController:GetAllWallpapers');


//venues controller
$app->get('/venues' , 'VenuesController:TestFn');
$app->post('/venues/create' , 'VenuesController:AddNewVenue');
$app->get('/venues/get/{venue_id}' , 'VenuesController:GetVenueByID');
$app->get('/venues/get' , 'VenuesController:GetAllVenues');
$app->get('/venues/get/claimed/{venue_claimed}' , 'VenuesController:GetClaimedVenues');
$app->get('/venues/get/active/{venue_status}' , 'VenuesController:GetAllActiveVenues');
$app->get('/venues/types' , 'VenuesController:GetVenueTypes');
$app->get('/venues/times/admin/{venue_id}' , 'VenuesController:GetTimmings');
$app->post('/venues/update/time/{venue_id}' , 'VenuesController:UpdateTimmings');
$app->post('/venues/update/information/{venue_id}' , 'VenuesController:UpdateVenues');
$app->get('/venues/now/{open}/{close}' , 'VenuesController:GetOpenVenues');
$app->post('/venues/update/type' , 'VenuesController:UpdateVenueTypeName');
$app->delete('/venues/del' , 'VenuesController:DeleteVenue');




//venue events api 
$app->get('/events/get' , 'EventsController:GetAllEventsAdmin');
$app->get('/events/createdetails' , 'EventsController:GetVenuesEvents');
$app->post('/events/create' , 'EventsController:CreateNewEvent');
$app->get('/events/special' , 'EventsController:SpecialEvetns');
$app->delete('/events/del' , 'VenuesController:DeleteEvent');
?>