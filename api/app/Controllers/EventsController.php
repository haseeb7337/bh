<?php

namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;

class EventsController{
    public function TestEvent(){
        echo "Hello Event";
    }


    //create a new event for the venu
    public function CreateNewEvent($request, $response) {
        $db = getDB();

        //get the picture data
        $fileName = $_FILES['afile']['name'];
        $fileType = $_FILES['afile']['type'];
        $fileContent = file_get_contents($_FILES['afile']['tmp_name']);
        $dataUrl = 'data:' . $fileType . ';base64,' . base64_encode($fileContent);

       // echo $fileName;
       //getting event data
        $data = $request->getParsedBody();
        $venue_event_title = $data['venue_event_title'];
       // $venue_event_pic = $data['venue_event_pic'];
        $venue_event_pic_cap ="";
        $venue_event_descp = $data['venue_event_descp'];
        $venue_event_start_date = $data['venue_event_start_date'];
        $venue_event_end_date = $data['venue_event_end_date'];
        $venue_event_multi_dates = $data['venue_event_multi_dates'];
        $venue_event_start_time = $data['venue_event_start_time'];
        $venue_event_end_time = $data['venue_event_end_time'];
        $event_type_cat_id = $data['event_type_cat_id'];
        $venue_id = $data['venue_id'];
        $is_event_active = $data['is_event_active'];



        $uploaddir =  __DIR__ . '/uploads/';
        //getting the directory to file to transfer
        $uploadfile = $uploaddir . basename($_FILES['afile']['name']);
        //getting the image name 
        $userImageName = basename( $_FILES['afile']['name']);

         //saving the file on the server
         if (move_uploaded_file($_FILES['afile']['tmp_name'], $uploadfile)) {
            $insertStatement = $db->insert(array('venue_event_title', 'venue_event_pic', 'venue_event_pic_cap', 'venue_event_descp', 'venue_event_start_date', 'venue_event_end_date', 'venue_event_multi_dates', 'venue_event_start_time', 'venue_event_end_time', 'event_type_cat_id', 'venue_id', 'is_event_active'))->into('bh_venues_event')->values(array($venue_event_title, $userImageName, $venue_event_pic_cap, $venue_event_descp, $venue_event_start_date, $venue_event_end_date, $venue_event_multi_dates, $venue_event_start_time, $venue_event_end_time, $event_type_cat_id, $venue_id, $is_event_active));
            $insertId = $insertStatement->execute(false);
            if ($insertId) {
                $data = array('created' => 'Success', 'msg' => 'New Event has been created', 'status' => 201);
                return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            } else {
                $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
         }//end of file upload 
    }

    //update event
    public function UpdateEvent(){

    }

    //create event through facebook
    public function CreateFacebookEvent(){

    }

    //get all events admin
    public function GetAllEventsAdmin($request , $response){
        $db = getDB();
            
        //getting the request body
	
		$loginUser = "SELECT
                bh_venues.venue_name,
                bh_venues_event.venue_event_id,
                bh_venues_event.venue_event_title,
                bh_venues_event.venue_event_pic,
                bh_venues_event.venue_event_pic_cap,
                bh_venues_event.venue_event_descp,
                bh_venues_event.venue_event_start_date,
                bh_venues_event.venue_event_end_date,
                bh_venues_event.venue_event_multi_dates,
                bh_venues_event.venue_event_start_time,
                bh_venues_event.venue_event_end_time,
                bh_venues_event.is_event_active,
                bh_event_type_cat.event_type_cat_name
                FROM
                bh_venues_event
                INNER JOIN bh_venues ON bh_venues_event.venue_id = bh_venues.venue_id
                INNER JOIN bh_event_type_cat ON bh_venues_event.event_type_cat_id = bh_event_type_cat.event_type_cat_id";
		try
		{
			$stmt = $db->prepare($loginUser);
			$stmt->execute();
			$users = $stmt->fetchAll();
			$db = null;
			$max = sizeof($users);
			//echo '{"result":'.json_encode($users).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($users).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'There are no Active Venue Types', 'status' => 201);
				return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    //get a single event on event id
    public function GetEventById(){

    }

    //get event by venue id
    public function GetEventByVenueID(){

    }

    //get events for users
    public function GetAllActiveEventsUser(){

    }

    //change evetn status active inactive
    public function ChangeEventStatus(){
        
    }

    //get the special events for the users
    public function SpecialEvetns($request , $response){
        $db = getDB();
            
        //getting the request body
	
		$loginUser = "SELECT
                bh_venues_event.venue_event_id,
                bh_venues_event.venue_event_title,
                bh_venues_event.venue_event_pic,
                bh_venues_event.venue_event_pic_cap,
                bh_venues_event.venue_event_descp,
                bh_venues_event.venue_event_start_date,
                bh_venues_event.venue_event_end_date,
                bh_venues_event.venue_event_multi_dates,
                bh_venues_event.venue_event_start_time,
                bh_venues_event.venue_event_end_time,
                bh_venues_event.is_event_active,
                bh_venues.venue_name
                FROM
                bh_venues_event
                INNER JOIN bh_venues ON bh_venues_event.venue_id = bh_venues.venue_id
                INNER JOIN bh_event_type_cat ON bh_venues_event.event_type_cat_id = bh_event_type_cat.event_type_cat_id
                WHERE
                bh_event_type_cat.event_type_cat_name = 'Special Event'";
		try
		{
			$stmt = $db->prepare($loginUser);
			$stmt->execute();
			$users = $stmt->fetchAll();
			$db = null;
			$max = sizeof($users);
			//echo '{"result":'.json_encode($users).'}';
			if($max > 0){
			//returning response back
			return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
			->write('{"result":'.json_encode($users).'}'); 
			}else{
				$data = array('access' => 'forbidden', 'msg' => 'There are no Active Venue Types', 'status' => 201);
				return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
			}
		
		}
		catch (PDOException $exception)
		{
			echo '{"error":{"result":'. $exception->getMessage() .'}}';
		}
    }

    public function DeleteEvent($request , $response)
    {
        $db = getDB();
        $data = $request->getParsedBody();
        $venue_event_id = $data['venue_event_id'];

        $delVenue = "DELETE FROM `bh_venues_event` WHERE `venue_event_id` = :venue_event_id";
        $stmt = $db->prepare($delVenue);
        $stmt->bindParam("venue_event_id", $venue_event_id);
        $deluser = $stmt->execute();
        $db = null;
        if ($deluser) {
            $data = array('msg' => 'Venue has been deleted', 'status' => 200);
            return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        } else {
            $data = array('msg' => 'There was an error deleting the venue', 'status' => 400);
            return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        }
    }

    

    //get list of venues and event categories for adding evernt
    public function GetVenuesEvents($request , $response)
    {
        $db = getDB();
        $getVenues = "SELECT
                    bh_venues.venue_id,
                    bh_venues.venue_name
                    FROM
                    bh_venues";
        
        $getEventsCat = "SELECT
                bh_event_type_cat.event_type_cat_id,
                bh_event_type_cat.event_type_cat_name
                FROM
                bh_event_type_cat";

        try
        {
            $stmt = $db->prepare($getVenues);
            $stmt->execute();
            $venues = $stmt->fetchAll();

            $max = sizeof($venues);

            $stmt_s = $db->prepare($getEventsCat);
            $stmt_s->execute();
            $events = $stmt_s->fetchAll();
            $db = null;
        
            

            $ar1 = array("eventypes" =>$events);
            $ar2 = array("venues" => $venues);

            $result = array_merge_recursive($ar1, $ar2);
            //echo '{"result":'.json_encode($projects).'}';
            if($max > 0){
            //returning response back
            return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
            ->write('{"result":'.json_encode($result).'}'); 
            }else{
                $data = array('access' => 'forbidden', 'msg' => 'No Project Currently Active', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }

        }
        catch (PDOException $exception)
        {
            echo '{"error":{"result":'. $exception->getMessage() .'}}';
        }
    }
}

?>