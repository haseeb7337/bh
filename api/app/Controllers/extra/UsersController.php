<?php

namespace App\Controllers;
require __DIR__  . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;


class UsersController
{
    //user login 
    public function UserLogin($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            $db = getDB();
            
            //getting the request body
            $data = $request->getParsedBody();
            $email = $data['email'];
            $password = $data['password'];

            //select query  
            $selectStatement = $db->select(array('user_id' , 'fullname' , 'photo' , 'date_joined'))
            ->from('tbl_users')->whereMany(array('email' => $email, 'password' => $password), '=');
            
            //executing the statement
            $stmt = $selectStatement->execute();
            $data = $stmt->fetchAll();
            $db = null;

            //getting the size of array
            $max = sizeof($data);
            if($max > 0){
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')
                ->write('{"result":'.json_encode($data).'}'); 
            }else{
                $data = array('access' => 'forbidden', 'msg' => 'Your login details are incorrect', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
            }
            

          
        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
        //return 'user login';
    }

    //user registration
    public function UserRegistration($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            //getting the request
            $data = $request->getParsedBody();

            //getting the parameters form the request
            $name = $data['name'];
            $email = $data['email'];
            $password = $data['password'];

            //getting the uploaded file details.
            if (!isset($_FILES['uploads'])) { 
                 $data = array('registration' => 'Failed', 'msg' => 'Please chose a profile picture', 'status' => 415);
                return $response->withStatus(415)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
            }
            
            //the file was uploaded successfully
            else{ 
                //check the file type
                $type = $_FILES['uploads']['type'];
                
                //the type is ok we will process our request
                if($type == "image/jpeg" || $type == "image/png"){
                                
                    //getting the upload directory
                    $uploaddir =  __DIR__ . '/uploads/';
                    
                    //getting the directory to file to transfer
                    $uploadfile = $uploaddir . basename($_FILES['uploads']['name']);

                    //getting the image name 
                    $userImageName = basename($_FILES['uploads']['name']);

                    //trying to move the file to destination folder
                    if (move_uploaded_file($_FILES['uploads']['tmp_name'], $uploadfile)) {
                        //echo "File is valid, and was successfully uploaded.\n";

                        //adding the infromation to the database
                        $db = getDB();
                        
                        //checking if email is already used
                        $selectStatement = $db->select(array('user_id'))->from('tbl_users')->where('email', '=', $email);
                        $stmt = $selectStatement->execute();
                        $data = $stmt->fetchAll();
                        $size = sizeof($data);

                        //if email already exists show user an error
                        if($size > 0)
                        {
                            $data = array('registration' => 'Failed', 'msg' => 'Email already exits. Want to try with different email?', 'status' => 409);
                            return $response->withStatus(409)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
                        }

                        //else save the user information to the database
                        else
                        {
                            //insert query for the user
                            $insertStatement = $db->insert(array('fullname', 'email', 'password' , 'photo'))
                            ->into('tbl_users')
                            ->values(array($name, $email, $password , $userImageName));
                            
                            //executing the statement
                            $insertId = $insertStatement->execute(false);
                            if($insertId){
                                $data = array('registration' => 'Success', 'msg' => 'Your account has been registered successfully.', 'status' => 201);
                                return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));   
                            }else{
                                $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                                return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
                            }
                        }
                    }//if condition ends for moving the file 
                    else {
                        //give error message to user if file was not uploaded 
                        $data = array('registration' => 'Failed', 'msg' => 'Unable to upload your picture. Please try again', 'status' => 415);
                        return $response->withStatus(415)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
                    }
                }
                //the image type is not ok and we need to throw an error to the user
                else{   
                    $data = array('registration' => 'Failed', 'msg' => 'You have chosen a wrong File. We support JPG/JPEG and PNG only.', 'status' => 400);
                    return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));  
                }  
            }
        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    //activate deactivate user 
    public function UserActDec($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
               
        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    //list all users
    public function ListAllUsers($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            $db = getDB();
            $selectStatement = $db->select()->from('tbl_users');
            $stmt = $selectStatement->execute();
            $data = $stmt->fetchAll();
            return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write('{"result":'.json_encode($data).'}');    
        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }

    //block or unblock a user 
    public function BlockUser($request , $response)
    {
        $a = verifyApiKey($request);
        if($a)
        {
            $db = getDB();
            //getting the get varibales form the request
            //$allGetVars = $request->getQueryParams();
            $userid = $request->getAttribute('userid');
            $userStatus = $request->getAttribute('usrstatus');

          
            //update query for the user unblock
            $updateStatement = $db->update(array('status' => $userStatus))
                       ->table('tbl_users')
                       ->where('user_id', '=', $userid);
            $affectedRows = $updateStatement->execute();
            $db = null;
            if($affectedRows){
                $data = array('msg' => 'User status changed', 'status' => 200);
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }else{
                $data = array('msg' => 'Unable to update the status. Try again later', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
            
        }
        else
        {
            $data = array('access' => 'forbidden', 'msg' => 'You are not Authorized', 'status' => 403);
            return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        }
    }
}

?>