<?php
namespace App\Controllers;
require __DIR__ . '/../Respone/response.php';
require __DIR__ . '/../../bootstrap/config.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\UploadedFileInterface as Files;
class VenuesController {
    public function TestFn() {
        echo "Hello world";
    }
    //adding a new venue
    public function AddNewVenue($request, $response) {
        $data = $request->getParsedBody();



        $fileName = $_FILES['afile']['name'];
        $fileType = $_FILES['afile']['type'];
        $fileContent = file_get_contents($_FILES['afile']['tmp_name']);
        $dataUrl = 'data:' . $fileType . ';base64,' . base64_encode($fileContent);


       // echo $fileType;
       

        $venue_name = $data['venue_name'];
        $venue_desc = $data['venue_desc'];
        $venue_type_id = $data['venue_type_id'];
        $venue_street = $data['venue_street'];
        $venue_state = $data['venue_state'];
        $venue_city = $data['venue_city'];
        $venue_phone = $data['venue_phone'];
        $venue_twitter = $data['venue_twitter'];
        $venue_insta = $data['venue_insta'];
        $venue_facebook = $data['venue_facebook'];
        $venue_youtube = $data['venue_youtube'];
        $venue_website = $data['venue_website'];
        $venue_email = $data['venue_email'];
      
        //timming
        $mopen = $data['mopen']; //;
        $mclose = $data['mclose'];
        $topen = $data['topen'];
        $tclose = $data['tclose'];
        $wedopen = $data['wedopen'];
        $wedclose = $data['wedclose'];
        $thopen = $data['thopen'];
        $thclose = $data['thclose'];
        $friopen = $data['friopen'];
        $friclose = $data['friclose'];
        $satopen = $data['satopen'];
        $satclose = $data['satclose'];
        $sunopen = $data['sunopen'];
        $sunclose = $data['sunclose'];
      
        $db = getDB();


        //check the file type
       // $fileName = $_FILES['afile']['name'];
      //  $fileType = $_FILES['afile']['type'];


       // echo $fileName;

        //if($fileType == "image/jpeg" || $fileType == "image/png"){  

            //starting uploads here

            //getting the upload directory
            $uploaddir =  __DIR__ . '/uploads/';

            //getting the directory to file to transfer

           
            $uploadfile = $uploaddir . basename($_FILES['afile']['name']);

            //getting the image name 
            $userImageName = basename( $_FILES['afile']['name']);

             //saving the file on the server
             if (move_uploaded_file($_FILES['afile']['tmp_name'], $uploadfile)) {
                $insertStatement = $db->insert(array('venue_name', 'venue_desc', 'venue_type_id', 'venue_street', 'venue_state', 'venue_city', 'venue_phone', 'venue_twitter', 'venue_insta', 'venue_facebook', 'venue_youtube', 'venue_website', 'venue_email' , 'venue_image'))->into('bh_venues')->values(array($venue_name, $venue_desc, $venue_type_id, $venue_street, $venue_state, $venue_city, $venue_phone, $venue_twitter, $venue_insta, $venue_facebook, $venue_youtube, $venue_website, $venue_email , $userImageName));
                $insertId = $insertStatement->execute(false);
                $id = $db->lastInsertId();
                if ($insertId) {
                    $savetime = $db->insert(array('venue_id', 'monday_open', 'monday_close', 'tuesday_open', 'tuesday_close', 'wednesday_open', 'wednesday_close', 'thursday_open', 'thursday_close', 'friday_open', 'friday_close', 'saturday_open', 'saturday_close', 'sunday_open', 'sunday_close'))->into('bh_venue_timmings')->values(array($id, $mopen, $mclose, $topen, $tclose, $wedopen, $wedclose, $thopen, $thclose, $friopen, $friclose, $satopen, $satclose, $sunopen, $sunclose));
                    $checkinsert = $savetime->execute(false);
                    if ($checkinsert) {
                        $data = array('registration' => 'Success', 'msg' => 'Your Venue has been registered successfully.', 'status' => 201, 'id' => $id);
                        return $response->withStatus(201)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
                    } else {
                        $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                        return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
                    }
                } else {
                    $data = array('registration' => 'Failed', 'msg' => 'An unknow error occured. Please try again later.', 'status' => 403);
                    return $response->withStatus(403)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
                }
             }//end of if uploading file move folder
             else
             {
                 $data = array('upload' => 'Failed', 'msg' => 'Unable to upload your picture. Please try again', 'status' => 415);
                 return $response->withStatus(415)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
             }

            //end uploads here
       // }//end of image wala if condition
        // else{
        //     $data = array('upload' => 'Failed', 'msg' => 'You have chosen a wrong File. We support JPG/JPEG and PNG only.', 'status' => 400);
        //     return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data)); 
        // }
            


       
    }
    //get venue by Id will be used while viewing the single venu
    //or when we want to update th venue
    public function GetVenueByID($request, $response) {
        $db = getDB();
        //getting the request body
        $venue_id = $request->getAttribute('venue_id');
        $loginUser = "SELECT
                        bh_venues.venue_id,
                        bh_venues.venue_name,
                        bh_venues.venue_street,
                        bh_venues.venue_state,
                        bh_venues.venue_city,
                        bh_venues.venue_desc,
                        bh_venues.venue_type_id,
                        bh_venues.venue_phone,
                        bh_venues.venue_twitter,
                        bh_venues.venue_insta,
                        bh_venues.venue_facebook,
                        bh_venues.venue_youtube,
                        bh_venues.venue_website,
                        bh_venues.venue_email,
                        bh_venues.venue_image,
                        bh_venues.venue_created_date,
                        TIME_FORMAT(bh_venue_timmings.monday_open, '%h:%i %p') as monday_open,
                        TIME_FORMAT(bh_venue_timmings.monday_close, '%h:%i %p') as monday_close,
                        TIME_FORMAT(bh_venue_timmings.tuesday_open, '%h:%i %p') as tuesday_open,
                        TIME_FORMAT(bh_venue_timmings.tuesday_close, '%h:%i %p') as tuesday_close,
                        TIME_FORMAT(bh_venue_timmings.wednesday_open, '%h:%i %p') as wednesday_open,
                        TIME_FORMAT(bh_venue_timmings.wednesday_close, '%h:%i %p') as wednesday_close,
                        TIME_FORMAT(bh_venue_timmings.thursday_open, '%h:%i %p') as thursday_open,
                        TIME_FORMAT(bh_venue_timmings.thursday_close, '%h:%i %p') as thursday_close,
                        TIME_FORMAT(bh_venue_timmings.friday_open, '%h:%i %p') as friday_open,
                        TIME_FORMAT(bh_venue_timmings.friday_close, '%h:%i %p') as friday_close,
                        TIME_FORMAT(bh_venue_timmings.saturday_open, '%h:%i %p') as saturday_open,
                        TIME_FORMAT(bh_venue_timmings.saturday_close, '%h:%i %p') as saturday_close,
                        TIME_FORMAT(bh_venue_timmings.sunday_open, '%h:%i %p') as sunday_open,
                        TIME_FORMAT(bh_venue_timmings.sunday_close, '%h:%i %p') as sunday_close,
                        bh_venue_types.venue_type_name
                        FROM
                        bh_venues
                        INNER JOIN bh_venue_timmings ON bh_venues.venue_id = bh_venue_timmings.venue_id
                        INNER JOIN bh_venue_types ON bh_venues.venue_type_id = bh_venue_types.venue_type_id
                        WHERE
                        bh_venues.venue_id = :venue_id";

            $getVenuTypes = "SELECT
                        bh_venue_types.venue_type_id,
                        bh_venue_types.venue_type_name,
                        bh_venue_types.venue_type_created
                        FROM
                        bh_venue_types";
            
        try {

            //get venue by id
            $stmt = $db->prepare($loginUser);
            $stmt->bindParam("venue_id", $venue_id);
            $stmt->execute();
            $users = $stmt->fetchAll();
          
            //get venue types
            $stmts = $db->prepare($getVenuTypes);
            $stmts->execute();
            $types = $stmts->fetchAll();


            $ar1 = array("details" =>$users);
            $ar2 = array("types" => $types);
            $result = array_merge_recursive($ar1, $ar2);

           $max = sizeof($users);
            
            
            
            //echo '{"result":'.json_encode($users).'}';
            if ($max > 0) {
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write('{"result":' . json_encode($result) . '}');
            } else {
                $data = array('access' => 'forbidden', 'msg' => 'Your login details are incorrect', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
        }
        catch(PDOException $exception) {
            echo '{"error":{"result":' . $exception->getMessage() . '}}';
        }
    }
    //get all venues admin
    public function GetAllVenues($request, $response) {
        $db = getDB();
        //getting the request body
        //$venue_id = $request->getAttribute('venue_id');
        $loginUser = "SELECT
                        bh_venues.venue_id,
                        bh_venues.venue_name,
                        bh_venues.venue_desc,
                        bh_venues.venue_street,
                        bh_venues.venue_state,
                        bh_venues.venue_city,
                        bh_venues.venue_phone,
                        bh_venues.venue_twitter,
                        bh_venues.venue_insta,
                        bh_venues.venue_facebook,
                        bh_venues.venue_youtube,
                        bh_venues.venue_website,
                        bh_venues.venue_email,
                        bh_venues.venue_image,
                        bh_venues.is_venue_claimed,
                        bh_venues.venue_status,
                        bh_venues.venue_created_date,
                        bh_venue_types.venue_type_name,
                        bh_users.user_name AS claimed_by
                        FROM
                        bh_venues
                        LEFT JOIN bh_users ON bh_venues.user_id = bh_users.user_id
                        INNER JOIN bh_venue_types ON bh_venues.venue_type_id = bh_venue_types.venue_type_id ORDER BY venue_id DESC";
        try {
            $stmt = $db->prepare($loginUser);
            $stmt->execute();
            $users = $stmt->fetchAll();
            $db = null;
            $max = sizeof($users);
            //echo '{"result":'.json_encode($users).'}';
            if ($max > 0) {
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write('{"result":' . json_encode($users) . '}');
            } else {
                $data = array('access' => 'forbidden', 'msg' => 'Your login details are incorrect', 'status' => 400);
                return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
        }
        catch(PDOException $exception) {
            echo '{"error":{"result":' . $exception->getMessage() . '}}';
        }
    }
    //get all claimed venues admin
    public function GetClaimedVenues($request, $response) {
        $db = getDB();
        //getting the request body
        $venue_claimed = $request->getAttribute('venue_claimed');
        $loginUser = "SELECT
                        bh_venues.venue_id,
                        bh_venues.venue_name,
                        bh_venues.venue_desc,
                        bh_venues.venue_street,
                        bh_venues.venue_state,
                        bh_venues.venue_city,
                        bh_venues.venue_phone,
                        bh_venues.venue_twitter,
                        bh_venues.venue_insta,
                        bh_venues.venue_facebook,
                        bh_venues.venue_youtube,
                        bh_venues.venue_website,
                        bh_venues.venue_email,
                        bh_venues.venue_image,
                        bh_venues.is_venue_claimed,
                        bh_venues.venue_status,
                        bh_venues.venue_created_date,
                        bh_venue_types.venue_type_name,
                        bh_users.user_name AS claimed_by
                        FROM
                        bh_venues
                        LEFT JOIN bh_users ON bh_venues.user_id = bh_users.user_id
                        INNER JOIN bh_venue_types ON bh_venues.venue_type_id = bh_venue_types.venue_type_id
                        WHERE
                        bh_venues.is_venue_claimed = :venue_claimed";
        try {
            $stmt = $db->prepare($loginUser);
            $stmt->bindParam("venue_claimed", $venue_claimed);
            $stmt->execute();
            $users = $stmt->fetchAll();
            $db = null;
            $max = sizeof($users);
            //echo '{"result":'.json_encode($users).'}';
            if ($max > 0) {
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write('{"result":' . json_encode($users) . '}');
            } else {
                $data = array('access' => 'forbidden', 'msg' => 'There are no Claimed Venues', 'status' => 201);
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
        }
        catch(PDOException $exception) {
            echo '{"error":{"result":' . $exception->getMessage() . '}}';
        }
    }
    //get all active venues Users
    public function GetAllActiveVenues($request, $response) {
        $db = getDB();
        //getting the request body
        $venue_status = $request->getAttribute('venue_status');
        $loginUser = "SELECT
                        bh_venues.venue_id,
                        bh_venues.venue_name,
                        bh_venues.venue_desc,
                        bh_venues.venue_street,
                        bh_venues.venue_state,
                        bh_venues.venue_city,
                        bh_venues.venue_phone,
                        bh_venues.venue_twitter,
                        bh_venues.venue_insta,
                        bh_venues.venue_facebook,
                        bh_venues.venue_youtube,
                        bh_venues.venue_website,
                        bh_venues.venue_email,
                        bh_venues.venue_image,
                        bh_venues.is_venue_claimed,
                        bh_venues.venue_created_date,
                        bh_venue_types.venue_type_name,
                        bh_users.user_name AS claimed_by
                        FROM
                        bh_venues
                        LEFT JOIN bh_users ON bh_venues.user_id = bh_users.user_id
                        INNER JOIN bh_venue_types ON bh_venues.venue_type_id = bh_venue_types.venue_type_id
                        WHERE
                        bh_venues.venue_status = :venue_status";
        try {
            $stmt = $db->prepare($loginUser);
            $stmt->bindParam("venue_status", $venue_status);
            $stmt->execute();
            $users = $stmt->fetchAll();
            $db = null;
            $max = sizeof($users);
            //echo '{"result":'.json_encode($users).'}';
            if ($max > 0) {
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write('{"result":' . json_encode($users) . '}');
            } else {
                $data = array('access' => 'forbidden', 'msg' => 'There are no Active Venues', 'status' => 201);
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
        }
        catch(PDOException $exception) {
            echo '{"error":{"result":' . $exception->getMessage() . '}}';
        }
    }
    //get list of venue times for admin to edit
    public function GetTimmings($request, $response) {
        $db = getDB();
        //getting the request body
        $venue_id = $request->getAttribute('venue_id');
        $loginUser = "SELECT
                    bh_venue_timmings.venue_time_id,
                    bh_venue_timmings.monday_open,
                    bh_venue_timmings.monday_close,
                    bh_venue_timmings.tuesday_open,
                    bh_venue_timmings.tuesday_close,
                    bh_venue_timmings.wednesday_open,
                    bh_venue_timmings.wednesday_close,
                    bh_venue_timmings.thursday_open,
                    bh_venue_timmings.thursday_close,
                    bh_venue_timmings.friday_open,
                    bh_venue_timmings.friday_close,
                    bh_venue_timmings.saturday_open,
                    bh_venue_timmings.saturday_close,
                    bh_venue_timmings.sunday_open,
                    bh_venue_timmings.sunday_close
                    FROM
                    bh_venue_timmings WHERE bh_venue_timmings.venue_id = :venue_id";
        try {
            $stmt = $db->prepare($loginUser);
            $stmt->bindParam("venue_id", $venue_id);
            $stmt->execute();
            $users = $stmt->fetchAll();
            $db = null;
            $max = sizeof($users);
            //echo '{"result":'.json_encode($users).'}';
            if ($max > 0) {
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write('{"result":' . json_encode($users) . '}');
            } else {
                $data = array('access' => 'forbidden', 'msg' => 'There are no Active Venues', 'status' => 201);
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
        }
        catch(PDOException $exception) {
            echo '{"error":{"result":' . $exception->getMessage() . '}}';
        }
    }
    public function UpdateTimmings($request, $response) {
        $db = getDB();
        $data = $request->getParsedBody();
        $venue_id = $request->getAttribute('venue_id');
        $mopen = $data['mopen'];
        $mclose = $data['mclose'];
        $topen = $data['topen'];
        $tclose = $data['tclose'];
        $wedopen = $data['wedopen'];
        $wedclose = $data['wedclose'];
        $thopen = $data['thopen'];
        $thclose = $data['thclose'];
        $friopen = $data['friopen'];
        $friclose = $data['friclose'];
        $satopen = $data['satopen'];
        $satclose = $data['satclose'];
        $sunopen = $data['sunopen'];
        $sunclose = $data['sunclose'];
        // $updateTimeQuery = "UPDATE `bh_venue_timmings`
        //                     SET    `monday_open` = :mopen,
        //                         `monday_close` = :mclose,
        //                         `tuesday_open` = :topen,
        //                         `tuesday_close` = :tclose,
        //                         `wednesday_open` = :wedopen,
        //                         `wednesday_close` = :wedclose,
        //                         `thursday_open` = :thopen,
        //                         `thursday_close` = :thclose,
        //                         `friday_open` = :friopen,
        //                         `friday_close` = :friclose,
        //                         `saturday_open` = :satopen,
        //                         `saturday_close` = :satclose,
        //                         `sunday_open` = :sunopen,
        //                         `sunday_close` = :sunclose
        //                     WHERE  venue_id = :venue_id ";
        $updateStatement = $db->update(array('monday_open' => $mopen, 'monday_close' => $mclose, 'tuesday_open' => $topen, 'tuesday_close' => $tclose, 'wednesday_open' => $wedopen, 'wednesday_close' => $wedclose, 'thursday_open' => $thopen, 'thursday_close' => $thclose, 'friday_open' => $friopen, 'friday_close' => $friclose, 'saturday_open' => $satopen, 'saturday_close' => $satclose, 'sunday_open' => $sunopen, 'sunday_close' => $sunclose))->table('bh_venue_timmings')->where('venue_id', '=', $venue_id);
        $affectedRows = $updateStatement->execute();
        if ($affectedRows) {
            $data = array('msg' => 'Timings have been updated', 'status' => 200);
            return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        } else {
            $data = array('msg' => 'There was an error updating the category', 'status' => 400);
            return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        }
    }
    // updating the venues by admin side
    public function UpdateVenues($request, $response) {
        $db = getDB();
        $data = $request->getParsedBody();
        $venue_id = $request->getAttribute('venue_id');
        $venue_name = $data['venue_name'];
        $venue_desc = $data['venue_desc'];
		$venue_type_id = $data['venue_type_id'];
        $venue_street = $data['venue_street'];
        $venue_state = $data['venue_state'];
        $venue_city = $data['venue_city'];
        $venue_phone = $data['venue_phone'];
        $venue_twitter = $data['venue_twitter'];
        $venue_insta = $data['venue_insta'];
        $venue_facebook = $data['venue_facebook'];
        $venue_youtube = $data['venue_youtube'];
        $venue_website = $data['venue_website'];
        $venue_email = $data['venue_email'];
        $venue_image = $data['venue_image'];
		
		
		if (empty($_FILES['ufile']['name'])) {
			// No file was selected for upload, your (re)action goes here
			$updateStatement = $db->update(array('venue_name' => $venue_name, 'venue_desc' => $venue_desc, 'venue_type_id' => $venue_type_id, 'venue_street' => $venue_street, 'venue_state' => $venue_state, 'venue_city' => $venue_city, 'venue_phone' => $venue_phone, 'venue_twitter' => $venue_twitter, 'venue_insta' => $venue_insta, 'venue_facebook' => $venue_facebook, 'venue_youtube' => $venue_youtube, 'venue_website' => $venue_website, 'venue_email' => $venue_email, 'venue_image' => $venue_image))->table('bh_venues')->where('venue_id', '=', $venue_id);
			$affectedRows = $updateStatement->execute();
			if ($affectedRows) {
				$data = array('msg' => 'Venue details have been updated', 'status' => 200);
				return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
			} else {
				$data = array('msg' => 'There was an error updating the category', 'status' => 400);
				return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
			}
		}
		else{
			$fileName = $_FILES['ufile']['name'];
			$fileType = $_FILES['ufile']['type'];
			$fileContent = file_get_contents($_FILES['ufile']['tmp_name']);
			$dataUrl = 'data:' . $fileType . ';base64,' . base64_encode($fileContent);
			
			 $uploaddir =  __DIR__ . '/uploads/';

            //getting the directory to file to transfer

           
            $uploadfile = $uploaddir . basename($_FILES['ufile']['name']);

            //getting the image name 
            $userImageName = basename( $_FILES['ufile']['name']);
			echo $userImageName;

             //saving the file on the server
             if (move_uploaded_file($_FILES['ufile']['tmp_name'], $uploadfile)) {
				 $updateStatement = $db->update(array('venue_name' => $venue_name, 'venue_desc' => $venue_desc, 'venue_street' => $venue_street, 'venue_state' => $venue_state, 'venue_city' => $venue_city, 'venue_phone' => $venue_phone, 'venue_twitter' => $venue_twitter, 'venue_insta' => $venue_insta, 'venue_facebook' => $venue_facebook, 'venue_youtube' => $venue_youtube, 'venue_website' => $venue_website, 'venue_email' => $venue_email, 'venue_image' => $userImageName))->table('bh_venues')->where('venue_id', '=', $venue_id);
				$affectedRows = $updateStatement->execute();
				if ($affectedRows) {
					$data = array('msg' => 'Venue details have been updated', 'status' => 200);
					return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
				} else {
					$data = array('msg' => 'There was an error updating the category', 'status' => 400);
					return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
				}
			 }
			
		}
	 
	 
       /* $fileType = $_FILES['ufile']['type'];
        $fileContent = file_get_contents($_FILES['ufile']['tmp_name']);
        $dataUrl = 'data:' . $fileType . ';base64,' . base64_encode($fileContent);
      /*  $updateStatement = $db->update(array('venue_name' => $venue_name, 'venue_desc' => $venue_desc, 'venue_street' => $venue_street, 'venue_state' => $venue_state, 'venue_city' => $venue_city, 'venue_phone' => $venue_phone, 'venue_twitter' => $venue_twitter, 'venue_insta' => $venue_insta, 'venue_facebook' => $venue_facebook, 'venue_youtube' => $venue_youtube, 'venue_website' => $venue_website, 'venue_email' => $venue_email))->table('bh_venues')->where('venue_id', '=', $venue_id);
        $affectedRows = $updateStatement->execute();
        if ($affectedRows) {
            $data = array('msg' => 'Venue details have been updated', 'status' => 200);
            return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        } else {
            $data = array('msg' => 'There was an error updating the category', 'status' => 400);
            return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        }*/
    }
    //update venue information will be based on venue and id and information
    public function UpdateVenueInfo($request, $response) {
    }
    //change venue status Active-Inactive only for admin
    public function UpdateVenuStatus($request, $response) {
    }
    //get my venues. the venues that have been claimed by user.
    //venue will be selected on user id base
    public function GetMyVenues($request, $response) {
    }
    //calim a venue. This is used when a user is going to claim the venue
    //A payment model has to be intergrated with it.
    public function ClaimVenue() {
    }
    public function GetVenueTypes($request, $response) {
        $db = getDB();
        //getting the request body
        $loginUser = "SELECT
                        bh_venue_types.venue_type_id,
                        bh_venue_types.venue_type_name,
						bh_venue_types.venue_type_created
                        FROM
                        bh_venue_types";
        try {
            $stmt = $db->prepare($loginUser);
            $stmt->execute();
            $users = $stmt->fetchAll();
            $db = null;
            $max = sizeof($users);
            //echo '{"result":'.json_encode($users).'}';
            if ($max > 0) {
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write('{"result":' . json_encode($users) . '}');
            } else {
                $data = array('access' => 'forbidden', 'msg' => 'There are no Active Venue Types', 'status' => 201);
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
        }
        catch(PDOException $exception) {
            echo '{"error":{"result":' . $exception->getMessage() . '}}';
        }
    }

    public function GetOpenVenues($request , $response)
    {
        
        $db = getDB();
        //getting the request body
        $open = $request->getAttribute('open');
        $close = $request->getAttribute('close');
        $loginUser = "SELECT
        bh_venues.venue_id,
        bh_venues.venue_name,
        bh_venues.venue_desc,
        bh_venues.venue_street,
        bh_venues.venue_state,
        bh_venues.venue_city,
        bh_venues.venue_image,
        bh_venue_timmings.venue_time_id,
        bh_venue_timmings.monday_open,
        bh_venue_timmings.monday_close,
        bh_venue_timmings.tuesday_open,
        bh_venue_timmings.tuesday_close,
        bh_venue_timmings.wednesday_open,
        bh_venue_timmings.wednesday_close,
        bh_venue_timmings.thursday_open,
        bh_venue_timmings.thursday_close,
        bh_venue_timmings.friday_open,
        bh_venue_timmings.friday_close,
        bh_venue_timmings.saturday_open,
        bh_venue_timmings.saturday_close,
        bh_venue_timmings.sunday_open,
        bh_venue_timmings.sunday_close
        FROM
        bh_venues
                        INNER JOIN bh_venue_timmings ON bh_venues.venue_id = bh_venue_timmings.venue_id
                        WHERE ".$open." <= TIME(NOW()) AND ".$close." >= TIME(NOW())  ORDER BY ".$open." ASC";
        try {
            $stmt = $db->prepare($loginUser);
            $stmt->execute();
            $users = $stmt->fetchAll();
            $db = null;
            $max = sizeof($users);
            //echo '{"result":'.json_encode($users).'}';
            if ($max > 0) {
                //returning response back
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write('{"result":' . json_encode($users) . '}');
            } else {
                $data = array('access' => 'forbidden', 'msg' => 'There are no Active Venues', 'status' => '400');
                return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
            }
        }
        catch(PDOException $exception) {
            echo '{"error":{"result":' . $exception->getMessage() . '}}';
        }
    }

    //this function is being used to udpate the 
    //venue type name
    public function UpdateVenueTypeName($request , $response){
        $db = getDB();
        $data = $request->getParsedBody();
        $venue_type_name = $data['venue_type_name'];
        $venue_type_id = $data['venue_type_id'];
        $updateStatement = $db->update(array('venue_type_name' => $venue_type_name))->table('bh_venue_types')->where('venue_type_id', '=', $venue_type_id);
        $affectedRows = $updateStatement->execute();
        if ($affectedRows) {
            $data = array('msg' => 'Venue details have been updated', 'status' => 200);
            return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        } else {
            $data = array('msg' => 'There was an error updating the category', 'status' => 400);
            return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        }


    }

    public function DeleteVenue($request , $response)
    {
        $db = getDB();
        $data = $request->getParsedBody();
        $venue_id = $data['venue_id'];

        $delVenue = "DELETE FROM `bh_venues` WHERE `venue_id` = :venue_id";
        $stmt = $db->prepare($delVenue);
        $stmt->bindParam("venue_id", $venue_id);
        $deluser = $stmt->execute();
        $db = null;
        if ($deluser) {
            $data = array('msg' => 'Venue has been deleted', 'status' => 200);
            return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        } else {
            $data = array('msg' => 'There was an error deleting the venue', 'status' => 400);
            return $response->withStatus(400)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
        }

    }
} //end of the class

?>